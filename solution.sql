INSERT INTO albums (album_title, date_released, artist_id) VALUES ("The Album", "2020-10-2", 1);

INSERT INTO users (email, password, datetime_created) VALUES ("johnsmith@gmail.com", "passwordA", "2021-01-01 1");

INSERT INTO users (email, password, datetime_created) VALUES ("juandelacruz@gmail.com", "passwordB", "2021-01-01 2");

INSERT INTO users (email, password, datetime_created) VALUES ("janesmith@gmail.com", "passwordC", "2021-01-01 3");

INSERT INTO users (email, password, datetime_created) VALUES ("mariadelacruz@gmail.com", "passwordD", "2021-01-01 4");

INSERT INTO users (email, password, datetime_created) VALUES ("johndoe@gmail.com", "passwordE", "2021-01-01 5");



INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (1, "First Code", "Hello World!", "2021-01-02 1");

INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (1, "Second Code", "Hello Earth!", "2021-01-02 2");

INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (1, "Third Code", "Welcome to Mars!", "2021-01-02 3");

INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (1, "Fourth Code", "Bye bye solar system!", "2021-01-02 4");

UPDATE posts SET user_id = 1 WHERE title = "Second Code";

UPDATE posts SET user_id = 2 WHERE title = "Third Code";

UPDATE posts SET user_id = 4 WHERE title = "Fourth Code";


SELECT user_id, title, content FROM posts WHERE user_id = 1;

SELECT id, email, datetime_created FROM users;


UPDATE posts SET content = "Hello to the people of the Earth!" WHERE id = 2;


DELETE FROM users WHERE email = "johndoe@gmail.com";
